﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mkfile
{
    class Program
    {
        private static int kiloByte = 1024;

        static void Main(string[] args)
        {
            Console.WriteLine("Running file maker at {0}", DateTime.Now);

            if (args.Length < 2)
            {
                Console.WriteLine("Please indicate the number of files.");
                return;
            }

            try
            {
                var numberOfFiles = 1;
                int.TryParse(args[0], out numberOfFiles);
                var outputDirectory = args[1];
                var random= new Random(23);
                

                for (int i = 0; i < numberOfFiles; i++)

                {
                    var fileSize = random.Next(100);
                    var fileContent = new StringBuilder();
                    
                    for (int m = 0; m < fileSize; m++)
                    {
                        var fileLine = new StringBuilder();
                        for (int k = 0; k < kiloByte; k++)
                        {
                            fileLine.Append("1");
                        }
                        fileContent.AppendLine(fileLine.ToString());
                    }

                    var randomFileName = Path.GetRandomFileName();
                    var filePath = outputDirectory + Path.ChangeExtension(randomFileName, "txt");

                    File.WriteAllText(filePath, fileContent.ToString());

                    Console.WriteLine("File created: {0}", filePath);
                }
                Console.WriteLine("done at {0}", DateTime.Now);
            }
            catch (Exception ex)
            {
                Console.WriteLine("There was a problem: " + ex.Message);
            }
        }
    }
}
